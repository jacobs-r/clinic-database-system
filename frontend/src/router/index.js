import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Users from '../views/Users.vue'
import Admins from '../views/Admins.vue'
import Patients from '../views/Patients.vue'
import Doctors from '../views/Doctors.vue'
import Appointments from '../views/Appointments.vue'

//Used to define the base URL of all AJAX requests to the backend
//This is the URL where the Express server is listening for requests
//Made global to be used by the whole application
global.baseURL = 'http://localhost:3000';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/users',
    name: 'Users',
    component: Users
  },
  {
    path: '/admins',
    name: 'Admins',
    component: Admins
  },
  {
    path: '/patients',
    name: 'Patients',
    component: Patients
  },
  {
    path: '/doctors',
    name: 'Doctors',
    component: Doctors
  },
  {
    path: '/appointments',
    name: 'Appointments',
    component: Appointments
  }
]

const router = new VueRouter({
  routes
});

export default router;
