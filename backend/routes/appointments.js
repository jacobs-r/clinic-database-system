var express = require('express');
const mongodb = require('mongodb');
const mongoClient = require('mongodb').MongoClient;
const url = process.env.DBURL;
var router = express.Router();

var options = {
    replSet: {
        sslValidate: false,
    }
}

router.get('/get', (req, res) => {
    const findItems = async () => {
        const client = await mongodb.MongoClient.connect(url, options, { useNewUrlParser: true, useUnifiedTopology: true });
        const db = client.db('clinic-manager');
        const appointments = await db.collection('appointments').find({}).toArray();
        const doctors = await db.collection('doctors').find({}).toArray();
        const patients = await db.collection('patients').find({}).toArray();
        let results = [appointments, doctors, patients];
        res.send(results);
        client.close();
    }
    findItems();
});

router.post('/post', (req, res) => {
    let formAppointment = req.body;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let newAppointment = {
            location: formAppointment.location,
            date: formAppointment.date,
            time: formAppointment.time,
            doctor: formAppointment.doctor,
            patient: formAppointment.patient,
        }
        dbo.collection('appointments').insertOne(newAppointment, (error, result) => {
            if (error) throw error;
            db.close();
            res.end();
        });
    });
});

router.delete('/delete/:id', (req, res) => {
    let id = req.params.id;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let param = { _id: new mongodb.ObjectID(id) };
        dbo.collection('appointments').deleteOne(param, (error, result) => {
            if (error) throw error;
            db.close();
            res.end();
        });
    });
});

router.put('/update/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    let formAppointment = req.body;
    let updateInfo = {
        $set: {
            location: formAppointment.location,
            date: formAppointment.date,
            time: formAppointment.time,
            doctor: formAppointment.doctor,
            patient: formAppointment.patient
        }
    }
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        dbo.collection('appointments').updateOne(id, updateInfo, (error, result) => {
            if (error) throw error;
            db.close();
            res.end();
        });
    });
});

module.exports = router;