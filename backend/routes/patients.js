var express = require('express');
const mongodb = require('mongodb');
const mongoClient = require('mongodb').MongoClient;
const url = process.env.DBURL;
var router = express.Router();

var options = {
    replSet: {
        sslValidate: false,
    }
}

router.get('/get', (req, res) => {
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        dbo.collection('patients').find().toArray((error, result) => {
            if (error) throw error;
            db.close();
            res.send(JSON.stringify(result));
            res.end();
        });
    });
});

router.post('/post', (req, res) => {
    let formPatient = req.body;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let newPatient = {
            patientFirstName: formPatient.patientFirstName,
            patientLastName: formPatient.patientLastName,
            sex: formPatient.sex,
            address: formPatient.address,
            zip: formPatient.zip,
            patientPhone: formPatient.patientPhone,
            dob: formPatient.dob
        }
        dbo.collection('patients').insertOne(newPatient, (error, result) => {
            if (error) throw error;
            db.close();
            res.end();
        });
    });
});

router.delete('/delete/:id', (req, res) => {
    let id = req.params.id;
    let nameToDelete = req.body.patientFirstName + " " + req.body.patientLastName;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let param1 = { _id: new mongodb.ObjectID(id) };
        let param2 = { patient: nameToDelete };
        dbo.collection('patients').deleteOne(param1, (error, result) => {
            if (error) throw error;
            dbo.collection('appointments').deleteOne(param2, (error2, result2) => {
                if (error2) throw error2;
                db.close();
                res.end();
            });
        });
    });
});

router.put('/update/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    let formPatient = req.body;
    let updateInfo = {
        $set: {
            patientFirstName: formPatient.patientFirstName,
            patientLastName: formPatient.patientLastName,
            sex: formPatient.sex,
            address: formPatient.address,
            zip: formPatient.zip,
            patientPhone: formPatient.patientPhone,
            dob: formPatient.dob
        }
    }
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        dbo.collection('patients').findOne(id, (error3, result3) => {
            if (error3) throw error3;
            dbo.collection('patients').updateOne(id, updateInfo, (error, result) => {
                if (error) throw error;
                let param = { patient: result3.patientFirstName + " " + result3.patientLastName };
                let updateInfo2 = {
                    $set: {
                        patient: formPatient.patientFirstName + " " + formPatient.patientLastName,
                    }
                };
                dbo.collection('appointments').updateOne(param, updateInfo2, (error2, result2) => {
                    if (error2) throw error2;
                    db.close();
                    res.end();
                });
            });
        })
    });
});

// router.get('/search/:searchTerm', (req, res) => {
//     let searchTerm = req.params.searchTerm;
//     mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
//         if (err) throw err;
//         let dbo = db.db('clinic-manager');
//         dbo.collection('patients').find({ patientFirstName: searchTerm }).toArray((error, result) => {
//             if (error) throw error;
//             db.close();
//             res.end(JSON.stringify(result));
//         });
//     });
// });

module.exports = router;