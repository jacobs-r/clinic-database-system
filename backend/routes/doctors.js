var express = require('express');
const mongodb = require('mongodb');
const mongoClient = require('mongodb').MongoClient;
const url = process.env.DBURL;
var router = express.Router();

var options = {
    replSet: {
        sslValidate: false,
    }
}

router.get('/get', (req, res) => {
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        dbo.collection('doctors').find().toArray((error, result) => {
            if (error) throw error;
            db.close();
            res.send(JSON.stringify(result));
            res.end();
        });
    });
});

router.post('/post', (req, res) => {
    let formDoctor = req.body;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let newDoctor = {
            doctorFirstName: formDoctor.doctorFirstName,
            doctorLastName: formDoctor.doctorLastName,
            doctorPhone: formDoctor.doctorPhone,
            specialty: formDoctor.specialty
        }
        dbo.collection('doctors').insertOne(newDoctor, (error, result) => {
            if (error) throw error;
            db.close();
            res.end();
        });
    });
});

router.delete('/delete/:id', (req, res) => {
    let id = req.params.id;
    let nameToDelete = req.body.doctorFirstName + " " + req.body.doctorLastName;
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        let param1 = { _id: new mongodb.ObjectID(id) };
        let param2 = { doctor: nameToDelete };
        dbo.collection('doctors').deleteOne(param1, (error, result) => {
            if (error) throw error;
            dbo.collection('appointments').deleteOne(param2, (error2, result2) => {
                if (error2) throw error2;
                db.close();
                res.end();
            });
        });
    });
});

router.put('/update/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    let formDoctor = req.body;
    let updateInfo = {
        $set: {
            doctorFirstName: formDoctor.doctorFirstName,
            doctorLastName: formDoctor.doctorLastName,
            doctorPhone: formDoctor.doctorPhone,
            specialty: formDoctor.specialty
        }
    }
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('clinic-manager');
        dbo.collection('doctors').findOne(id, (error3, result3) => {
            if (error3) throw error3;
            dbo.collection('doctors').updateOne(id, updateInfo, (error, result) => {
                if (error) throw error;
                let param = { doctor: result3.doctorFirstName + " " + result3.doctorLastName };
                let updateInfo2 = {
                    $set: {
                        doctor: formDoctor.doctorFirstName + " " + formDoctor.doctorLastName,
                    }
                };
                dbo.collection('appointments').updateOne(param, updateInfo2, (error2, result2) => {
                    if (error2) throw error2;
                    db.close();
                    res.end();
                });
            });
        })
    });
});

module.exports = router;