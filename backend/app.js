var express = require('express');
var http = require('http');
var path = require('path');
var cookieParser = require('cookie-parser');
var cors = require('cors');
require('dotenv').config();

var app = express();

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var usersRouter = require('./routes/users');
var patientsRouter = require('./routes/patients');
var doctorsRouter = require('./routes/doctors');
var appointmentsRouter = require('./routes/appointments')

app.use(express.json());
app.use(cors());

app.use('/doctors', doctorsRouter);
app.use('/users', usersRouter);
app.use('/patients', patientsRouter);
app.use('/appointments', appointmentsRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;
