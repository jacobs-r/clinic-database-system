Modify .env DBURL string with database user and password to connect to database

**Run the following on both frontend and backend folders**

npm i

**To start the backend:**

npm start

**To start the frontend:**

npm run serve

**To access admin page use credentials:**

username: admin
password: admin

**To access user page use credentials:**

username: user
password: user